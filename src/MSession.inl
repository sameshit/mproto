//
//  MSession.inl
//  mproto
//
//  Created by Oleg on 28.03.13.
//
//

#ifndef mproto_MSession_inl
#define mproto_MSession_inl

inline const char*          MSession::GetIP()
{
    return inet_ntoa(_addr.sin_addr);
}

inline uint16_t             MSession::GetPort()
{
    return ntohs(_addr.sin_port);
}

inline const sockaddr_in*   MSession::GetSockAddr()
{
    return &_addr;
}

inline void                 MSession::SetUserData(void *udata)
{
    _udata = udata;
}

inline void*                MSession::GetUserData()
{
    return _udata;
}

inline uint32_t             MSession::GetConnectionTimeout()
{
    return _timeout;
}

inline std::stringstream&   MSession::GetDisconnectReason()
{
    return _disconnect_reason;
}

inline uint8_t              MSession::GetConnectionId()
{
    return _conn_id;
}

inline uint32_t             MSession::GetRtt()
{
    return _rtt;
}

inline const uint64_t&      MSession::GetPackedAddr()
{
    return _packed_addr;
}
#endif
