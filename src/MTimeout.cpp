//
//  MTimeout.cpp
//  mproto
//
//  Created by Oleg on 14.10.12.
//
//

#include "MTimeout.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

MTimeout::MTimeout(CoreObject *core)
:MPing(core)
{
    fast_new(_timeout_timer,core,500);
    _timeout_timer->OnTimer.Attach(this, &MTimeout::CheckTimeouts);
}

MTimeout::~MTimeout()
{
    MProtoStopTimeoutTimer();
}

void MTimeout::MProtoStopTimeoutTimer()
{
    if (_timeout_timer != NULL)
    {
        fast_delete(_timeout_timer);
        _timeout_timer = NULL;
    }
}

void MTimeout::CheckTimeouts()
{
    COTime now;
    queue<MSession *> del_q;
    
    for (auto it = SessionsBegin(); it != SessionsEnd(); ++it)
    {
        if (now - (*it).second->_read_time > (*it).second->GetConnectionTimeout())
            del_q.push((*it).second);
    }
    
    while (!del_q.empty())
    {
        del_q.front()->GetDisconnectReason() << "Session timeouted";
        Disconnect(del_q.front());
        del_q.pop();
    }
}