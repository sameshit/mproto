//
//  MReliable.cpp
//  mproto
//
//  Created by Oleg on 14.10.12.
//
//

#include "MReliable.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

MReliable::MReliable(CoreObject * core)
:MOrder(core)
{
    fast_new(_reliable_timer,_core,200);
    _reliable_timer->OnTimer.Attach(this, &MReliable::CheckReliability);
}

MReliable::~MReliable()
{
    fast_delete(_reliable_timer);
}

void MReliable::SendReliable(MProtoLib::MSession *session, uint8_t *data, uint16_t size)
{
    COTime now;
    uint8_t order_id[4];
    MRMessage *new_msg;
    
    ++session->_send_id;
    Utils::PutBe32(order_id, session->_send_id);
    SystemSend(session, MTRMsg, order_id, 4, data, size);
    
    fast_new(new_msg,_core,session->_send_id,data,size,now());
    session->_unacked_messages.insert(new_msg);
}

void MReliable::ProcessAck(uint8_t *data, ssize_t &size, MProtoLib::MSession *session)
{
    uint32_t order_id;
    set<MRMessage*>::iterator it;
    MRMessage *old_msg;
    
    if (size != 4)
    {
#ifdef MPROTO_DEBUG
        LOG "Invalid ACK size("<<size<<") from "<<session->GetIP()<<":"<<session->GetPort()<<". Disconnecting session."EL;
#endif
        session->GetDisconnectReason() << "Invalid ACK size("<<size<<") in MReliable";
        Disconnect(session);
        return;
    }
    
    order_id = Utils::GetBe32(data);
    MRMessage new_msg(_core,order_id,NULL,0);
    
    it = session->_unacked_messages.find(&new_msg);
    if (it == session->_unacked_messages.end())
    {
#ifdef MPROTO_DEBUG
        LOG "Received duplicate or invalid ACK("<<order_id<<") from "<<session->GetIP()<<":"<<session->GetPort()<<". Ignoring.."EL;
#endif
        return;
    }
    old_msg = (*it);
    fast_delete(old_msg);
    session->_unacked_messages.erase(it);
}

void MReliable::CheckReliability()
{
    set<MRMessage*>::iterator msg_it,tmp_it;
    MSession *session;
    COTime now;
    uint8_t order_id[4];
    
    for (auto it = SessionsBegin(); it != SessionsEnd(); it ++ )
    {
        session = (*it).second;
        for (msg_it = session->_unacked_messages.begin() ; msg_it != session->_unacked_messages.end();++msg_it)
        {
            if (now() - (*msg_it)->_sent_time > 200)
            {
#ifdef MPROTO_DEBUG
                LOG "Lost packet with id "<<(*msg_it)->_order_id<<" from "<<session->GetIP()<<":"<<session->GetPort() EL;
#endif
                Utils::PutBe32(order_id,  (*msg_it)->_order_id);
                SystemSend(session, MTRMsg,order_id, 4, (*msg_it)->_data, (*msg_it)->_size);
                (*msg_it)->_sent_time = now();
            }
        }
    }
}
