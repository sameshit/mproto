//
//  MPing.h
//  mproto
//
//  Created by Oleg on 13.10.12.
//
//

#ifndef __mproto__MPing__
#define __mproto__MPing__

#include "MSystemDatagrams.h"
#include "../../CoreObject/src/core/time/Timer.h"

namespace MProtoLib
{
    class MPing
    :public MSystemDatagram
    {
    public:
        MPing(CoreObjectLib::CoreObject *core);
        virtual ~MPing();
    private:
        CoreObjectLib::Timer *_ping_timer;
        void MustPing();
        void Ping(MSession* session);
        void MProtoStopPingTimer();        
        
        friend class MProto;
    };
    
    inline void MPing::Ping(MSession *session)
    {
        SystemSend(session, MTPing);
    }
}

#endif /* defined(__mproto__MPing__) */
