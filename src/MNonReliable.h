//
//  MNonReliable.h
//  mproto
//
//  Created by Oleg on 15.10.12.
//
//

#ifndef __mproto__MNonReliable__
#define __mproto__MNonReliable__

#include "MReliable.h"
namespace MProtoLib
{
    typedef struct CongestionInfo
    {
        uint64_t delay;
    }CongestionInfo;
    
    
    class MNonReliable
    :public MReliable
    {
    private:
        void ProcessNonReliable(uint8_t *data,ssize_t &size, MSession *session);
    public:
        MNonReliable(CoreObjectLib::CoreObject *core);
        virtual ~MNonReliable();
        
        bool SendNonReliable(MSession* session,uint8_t *data,ssize_t size);
        bool SendNonReliable(MSession* session,uint8_t *data1,ssize_t size1,uint8_t *data2,ssize_t size2);
        bool SendNonReliable(MSession* session,uint8_t *data1,ssize_t size1,uint8_t *data2,ssize_t size2, uint8_t *data3, ssize_t size3);
        MessageEvent OnNonReliableMessage;
    };
}

#endif /* defined(__mproto__MNonReliable__) */
