#ifndef __mproto__MSession__
#define __mproto__MSession__

#include "MRMessage.h"

namespace MProtoLib
{
    class MSession
    {
    private:
        CoreObjectLib::CoreObject   *_core;
        uint8_t                 _conn_id;
        struct sockaddr_in      _addr;
        CoreObjectLib::COTime   _read_time;
        uint32_t                _recv_id,_send_id;
        uint64_t                _packed_addr;
        uint64_t                _nr_shift;
        uint64_t                _min_delay;
        uint32_t                _timeout;
        uint32_t                _rtt;
        void                    *_udata;
        std::stringstream       _disconnect_reason;
        std::set<MRMessage*,OrderIdLess>     _unordered_messages;
        std::set<MRMessage*,OrderIdLess>     _unacked_messages;
    
        friend class MHandshake;
        friend class MPing;
        friend class MTimeout;
        friend class MOrder;
        friend class MReliable;
        friend class MNonReliable;
        friend class MRtt;
    public:
        MSession(CoreObjectLib::CoreObject *core,const struct sockaddr_in *);
        virtual ~MSession();
        
        static void MakePackedAddress(uint64_t *packed_addr,const struct sockaddr_in* addr);
        static void MakePackedAddress(uint64_t *packed_addr,const char *ip,const uint16_t &port);
        static void UnPackAddress(uint8_t *packed_addr,struct sockaddr_in *addr);
        
        const char*             GetIP();
		uint16_t                GetPort();
        const sockaddr_in*      GetSockAddr();
        void                    SetUserData(void *udata);
        void*                   GetUserData();
        uint32_t                GetConnectionTimeout();
        std::stringstream&      GetDisconnectReason();
        uint8_t                 GetConnectionId();
        uint32_t                GetRtt();
        const uint64_t&         GetPackedAddr();
    };
    #include "MSession.inl"
}


#endif
