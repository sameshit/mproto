//
//  MNonReliable.cpp
//  mproto
//
//  Created by Oleg on 15.10.12.
//
//

#include "MNonReliable.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

#ifndef MIN
#define MIN(a,b) a<b?a:b
#endif

MNonReliable::MNonReliable(CoreObject *core)
:MReliable(core)
{

}

MNonReliable::~MNonReliable()
{

}

bool MNonReliable::SendNonReliable(MProtoLib::MSession *session, uint8_t *data, ssize_t size)
{
    uint8_t time_data[8];
    COTime now;
    
    Utils::PutBe64(time_data, now());
    
    return SystemSend(session, MTNMsg, time_data, 8, data, size);
}

bool MNonReliable::SendNonReliable(MProtoLib::MSession *session, uint8_t *data1, ssize_t size1,uint8_t *data2,ssize_t size2)
{
    uint8_t time_data[8];
    COTime now;
    
    Utils::PutBe64(time_data, now());
    
    return SystemSend(session, MTNMsg, time_data, 8, data1, size1,data2,size2);
}

bool MNonReliable::SendNonReliable(MProtoLib::MSession *session, uint8_t *data1, ssize_t size1,uint8_t *data2,ssize_t size2,uint8_t *data3,ssize_t size3)
{
    uint8_t time_data[8];
    COTime now;
    
    Utils::PutBe64(time_data, now());
    
    return SystemSend(session, MTNMsg, time_data, 8, data1, size1,data2,size2,data3,size3);
}

void MNonReliable::ProcessNonReliable(uint8_t *data, ssize_t &size, MProtoLib::MSession *session)
{
    CongestionInfo info;
    COTime now;
    uint64_t send_time;
    uint64_t cur_delay;
    
    if (size <= 8)
    {
        session->GetDisconnectReason() << "Invalid size("<<size <<") of non-reliable message in MNonReliable";
        Disconnect(session);
        return;
    }
    
    send_time = Utils::GetBe64(data);
    if (session->_nr_shift == 0)
    {
        if (now() > send_time)
            session->_nr_shift = 100000;
        else
            session->_nr_shift = send_time - now() + 100000;
    }
    cur_delay = send_time + session->_nr_shift - now();
    
    if (session->_min_delay == 0)
        session->_min_delay = cur_delay;
    else
        session->_min_delay = MIN(session->_min_delay, cur_delay);
    
    info.delay = cur_delay - session->_min_delay;
    
    OnNonReliableMessage(session,&data[8],size-8);
}

