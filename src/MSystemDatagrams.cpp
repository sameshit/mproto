//
//  MSystemDatagrams.cpp
//  mproto
//
//  Created by Oleg on 11.06.13.
//
//

#include "MSystemDatagrams.h"

using namespace MProtoLib;
using namespace CoreObjectLib;
using namespace std;

MSystemDatagram::MSystemDatagram(CoreObject *core)
:MHandshake(core)
{

}

MSystemDatagram::~MSystemDatagram()
{

}

void MSystemDatagram::ParseSystemDatagram(MMessageType &type,uint8_t *data,ssize_t &size, MSession *session)
{
    switch (type)
    {
        case MTAck:
            ProcessAck(data,size,session);
        break;
        case MTNMsg:
            ProcessNonReliable(data, size, session);
        break;
        case MTRMsg:
            ProcessReliable(data, size, session);
        break;
        case MTRttRep:
            ProcessRttRep(data, size, session);
        break;
        case MTRttReq:
            ProcessRttReq(data, size, session);
        break;
        case MTPing:
        break;
        default:
            session->GetDisconnectReason()<<"Invalid mproto message type("<<type<<")";
            Disconnect(session);
        break;
    }
    UpdateReadTime(session);
}