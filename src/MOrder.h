//
//  MOrder.h
//  mproto
//
//  Created by Oleg on 14.10.12.
//
//

#ifndef __mproto__MOrder__
#define __mproto__MOrder__

#include "MTimeout.h"

namespace MProtoLib
{
    class MOrder
    :public MTimeout
    {
    private:
        void ProcessReliable(uint8_t *data,ssize_t &size, MSession *session);
    public:
        MOrder(CoreObjectLib::CoreObject *core);
        virtual ~MOrder();
        
        MessageEvent OnReliableMessage;
    };
}

#endif /* defined(__mproto__MOrder__) */
