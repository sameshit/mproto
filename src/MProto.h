//
//  MProto.h
//  mproto
//
//  Created by Oleg on 27.11.12.
//
//

#ifndef __mproto__MProto__
#define __mproto__MProto__

#include "MRtt.h"

namespace MProtoLib
{
    class LIBEXPORT MProto
    :public MRtt
    {
    private:
        bool _deleted;
    public:
        MProto(CoreObjectLib::CoreObject *core);
        virtual ~MProto();
        
        void MProtoDeleteFromLoop();
    };
}

#endif /* defined(__mproto__MProto__) */
