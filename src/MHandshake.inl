//
//  MHandshake.inl
//  mproto
//
//  Created by Oleg on 28.03.13.
//
//

#ifndef mproto_MHandshake_inl
#define mproto_MHandshake_inl

inline bool MHandshake::SystemSend(MProtoLib::MSession *session, MProtoLib::MMessageType type, uint8_t *data1, ssize_t size1,uint8_t *data2,ssize_t size2,uint8_t *data3, ssize_t size3,uint8_t *data4,ssize_t size4)
{
    return SendTo(&session->_conn_id, 1, (uint8_t*)&type, 1, data1, size1,data2,size2,data3,size3,data4,size4, (struct sockaddr*)&session->_addr, sizeof(struct sockaddr_in));
}

inline bool MHandshake::SystemSend(MProtoLib::MSession *session, MProtoLib::MMessageType type, uint8_t *data1, ssize_t size1,uint8_t *data2,ssize_t size2,uint8_t *data3, ssize_t size3)
{
    return SendTo(&session->_conn_id, 1, (uint8_t*)&type, 1, data1, size1,data2,size2,data3,size3, (struct sockaddr*)&session->_addr, sizeof(struct sockaddr_in));
}

inline bool MHandshake::SystemSend(MProtoLib::MSession *session, MProtoLib::MMessageType type, uint8_t *data1, ssize_t size1,uint8_t *data2,ssize_t size2)
{
    return SendTo(&session->_conn_id, 1, (uint8_t*)&type, 1, data1, size1,data2,size2, (struct sockaddr*)&session->_addr, sizeof(struct sockaddr_in));
}

inline bool MHandshake::SystemSend(MProtoLib::MSession *session, MProtoLib::MMessageType type, uint8_t *data, ssize_t size)
{
    return SendTo(&session->_conn_id, 1, (uint8_t*)&type, 1, data, size, (struct sockaddr*)&session->_addr, sizeof(struct sockaddr_in));
}

inline bool MHandshake::SystemSend(MProtoLib::MSession *session, MProtoLib::MMessageType type)
{
    return SendTo(&session->_conn_id, 1, (uint8_t*)&type, 1, (struct sockaddr*)&session->_addr, sizeof(struct sockaddr_in));
}

#endif
