//
//  MSystemDatagrams.h
//  mproto
//
//  Created by Oleg on 11.06.13.
//
//

#ifndef __mproto__MSystemDatagrams__
#define __mproto__MSystemDatagrams__

#include "MHandshake.h"

namespace MProtoLib
{
    class MSystemDatagram
    :public MHandshake
    {
    public:
        MSystemDatagram(CoreObjectLib::CoreObject *core);
        virtual ~MSystemDatagram();
    protected:
        virtual void ProcessReliable(uint8_t *data,ssize_t &size, MSession *session) = 0;
        virtual void ProcessNonReliable(uint8_t *data,ssize_t &size, MSession *session) = 0;
        virtual void ProcessAck(uint8_t *data,ssize_t &size,MSession *session) = 0;
        virtual void ProcessRttReq(uint8_t *data,ssize_t &size,MSession *session) = 0;
        virtual void ProcessRttRep(uint8_t *data,ssize_t &size,MSession *session) = 0;
        virtual void UpdateReadTime(MSession *session) = 0;
    private:
        void ParseSystemDatagram(MMessageType &type,uint8_t *data,ssize_t &size, MSession *session);
    };
}

#endif /* defined(__mproto__MSystemDatagrams__) */
