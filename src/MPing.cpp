//
//  MPing.cpp
//  mproto
//
//  Created by Oleg on 13.10.12.
//
//

#include "MPing.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

const uint32_t kPingPeriod = 200;

MPing::MPing(CoreObject* core)
:MSystemDatagram(core)
{
    fast_new(_ping_timer,_core,kPingPeriod);

    _ping_timer->OnTimer.Attach(this, &MPing::MustPing);
    OnConnect.Attach(this, &MPing::Ping);
}

MPing::~MPing()
{
    MProtoStopPingTimer();
}

void MPing::MProtoStopPingTimer()
{
    if (_ping_timer != NULL)
    {
        fast_delete(_ping_timer);
        _ping_timer = NULL;
    }
}

void MPing::MustPing()
{
    for (auto it = SessionsBegin(); it != SessionsEnd(); it ++)
        Ping((*it).second);
}
