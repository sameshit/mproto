//
//  MHandshake.cpp
//  mproto
//
//  Created by Oleg on 11.10.12.
//
//

#include "MHandshake.h"


using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

MHandshake::MHandshake(CoreObject *core)
:MIO(core)
{

}

MHandshake::~MHandshake()
{
    MSessionMapIterator it;
    MSession *session;
    
    for (it = _sessions_by_addr.begin(); it != _sessions_by_addr.end(); it++)
    {
        session = (*it).second;
        fast_delete(session);
    }
}

void MHandshake::ParseValidDatagram(uint8_t &conn_id,MMessageType &type,uint8_t *data, ssize_t &size, struct sockaddr_in *addr)
{
    uint64_t packed_addr;
    MSessionMapIterator it;
    pair<uint32_t, uint16_t> addr_pair;
    MSession *session;
    
    MSession::MakePackedAddress(&packed_addr, addr);
    
    it = _sessions_by_addr.find(packed_addr);
    if (it == _sessions_by_addr.end())
    {
        if (conn_id != 0) // Non-handshaked datagram. Skipping...
            return;
        fast_new(session,_core,addr);
        _sessions_by_addr.insert(make_pair(packed_addr, session));
        session->_conn_id = (rand() % 128) + 1;
        OnConnect(session);
    }
    else
    {
        session = (*it).second;
        
        if (conn_id == 0)
        {
            if (session->_conn_id == 0)
                session->_conn_id = 1;
            else if (!(session->_conn_id <= 128))
            {
                session->GetDisconnectReason() << "Connection reset in MHandshake";
                OnDisconnect(session);
                fast_delete(session);
                _sessions_by_addr.erase(it);
                fast_new(session,_core,addr);
                _sessions_by_addr.insert(make_pair(packed_addr, session));
                session->_conn_id = (rand() % 128) + 1;
                OnConnect(session);
            }
        }
        else if (conn_id <= 128)
        {
            if (session->_conn_id == conn_id)
                session->_conn_id += 127;
            else if (session->_conn_id == 0)
                session->_conn_id = conn_id;
            else if (session->_conn_id != conn_id + 127)
            {
                session->GetDisconnectReason() << "Invalid connection id in phase #2.1";
                OnDisconnect(session);
                _sessions_by_addr.erase(it);                
                fast_delete(session);
                return;
            }
        }
        else 
        {
            if (session->_conn_id + 127 == conn_id)
                session->_conn_id += 127;
            else if (session->_conn_id != conn_id)
            {
                session->GetDisconnectReason() << "Invalid connection id in phase #2.2";
                OnDisconnect(session);
                _sessions_by_addr.erase(it);                
                fast_delete(session);
                return;
            }
        }
    }
    
    if (_sessions_by_addr.find(packed_addr) != _sessions_by_addr.end())
        ParseSystemDatagram(type,data,size,session);
}

void MHandshake::Connect(const char *ip, uint16_t port)
{
	struct sockaddr_in addr;
    
    addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(ip);
    
    Connect(&addr);
}

void MHandshake::Connect(const struct sockaddr_in *addr)
{
    uint64_t packed_addr;
    MSession *session;
    MSessionMapIterator it;
    
    MSession::MakePackedAddress(&packed_addr, addr);
    
    it = _sessions_by_addr.find(packed_addr);
    if (it == _sessions_by_addr.end())
    {
        fast_new(session,_core, addr);
        _sessions_by_addr.insert(make_pair(packed_addr, session));
        OnConnect(session);
    }
}

bool MHandshake::Disconnect(const char *ip,uint16_t port)
{
	struct sockaddr_in addr;
    uint64_t packed_addr;
    MSessionMapIterator it;
    
    addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(ip);
    
    MSession::MakePackedAddress(&packed_addr, &addr);
    
    it = _sessions_by_addr.find(packed_addr);
    if (it == _sessions_by_addr.end())
    {
        COErr::Set("No such session in collection", -1);
        return false;
    }
    
    return Disconnect((*it).second);
}

bool MHandshake::Disconnect(MSession *session)
{
    uint64_t packed_addr;
    MSessionMapIterator it;

    MSession::MakePackedAddress(&packed_addr, &session->_addr);
    
    it = _sessions_by_addr.find(packed_addr);
    if (it!= _sessions_by_addr.end())
    {
        _sessions_by_addr.erase(it);        
        OnDisconnect(session);
        fast_delete(session);
        return true;
    }
    else
    {
        COErr::Set("No such session in collection", -1);
        return false;
    }
}

void MHandshake::Disconnect(const MSessionMapIterator &it)
{
    MSession *session = (*it).second;
    
    _sessions_by_addr.erase(it);
    OnDisconnect(session);
    fast_delete(session);
}