//
//  MProto.cpp
//  mproto
//
//  Created by Oleg on 27.11.12.
//
//

#include "MProto.h"
using namespace MProtoLib;
using namespace CoreObjectLib;

MProto::MProto(CoreObject *core)
:MRtt(core),_deleted(false)
{

}

MProto::~MProto()
{
    MProtoDeleteFromLoop();
}

void MProto::MProtoDeleteFromLoop()
{
    if (_deleted)
        return;
    
    MProtoStopRttTimer();
    MProtoStopTimeoutTimer();    
    MProtoStopPingTimer();
    DeleteFromLoop();
    _deleted = true;
}