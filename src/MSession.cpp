#include "MSession.h"

using namespace MProtoLib;
using namespace CoreObjectLib;
using namespace std;

MSession::MSession(CoreObject *core,const struct sockaddr_in *addr)
:_core(core),_addr(*addr),_conn_id(0),_recv_id(0),_send_id(0),_nr_shift(0),_min_delay(0),_udata(NULL),_timeout(2000),_rtt(1000)
{
    MakePackedAddress(&_packed_addr,&_addr);
}

MSession::~MSession()
{
    set<MRMessage*>::iterator it;
    MRMessage *message;
    
    for (it = _unordered_messages.begin(); it != _unordered_messages.end() ; ++it)
    {
        message = (*it);
        fast_delete(message);
    }
    
    for (it = _unacked_messages.begin(); it != _unacked_messages.end(); ++it)
    {
        message = (*it);
        fast_delete(message);
    }
}

void MSession::MakePackedAddress(uint64_t *packed_addr, const char *ip, const uint16_t &port)
{
    struct sockaddr_in addr;
    
    addr.sin_addr.s_addr = inet_addr(ip);
    addr.sin_port = htons(port);
    
    MakePackedAddress(packed_addr, &addr);
}

void MSession::MakePackedAddress(uint64_t *packed_addr, const struct sockaddr_in *addr)
{
    uint8_t  *p_addr;
    
    *packed_addr = 0;
    p_addr = (uint8_t *)packed_addr;
    memcpy(p_addr,&addr->sin_addr.s_addr,sizeof(struct in_addr));
    p_addr = &p_addr[sizeof(struct in_addr)];
    memcpy(p_addr,&addr->sin_port,sizeof(uint16_t));
}

void MSession::UnPackAddress(uint8_t *packed_addr, struct sockaddr_in *addr)
{
    uint8_t *pos;
    
    addr->sin_family = AF_INET;
    pos = packed_addr;
    memcpy(&addr->sin_addr.s_addr,pos,sizeof(struct in_addr));
    pos += sizeof(struct in_addr);
    memcpy(&addr->sin_port,pos,sizeof(uint16_t));
}