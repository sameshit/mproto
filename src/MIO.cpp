//
//  MIO.cpp
//  mproto
//
//  Created by Oleg on 11.10.12.
//
//

#include "MIO.h"

using namespace MProtoLib;
using namespace CoreObjectLib;

MIO::MIO(CoreObject *core)
:UdpSocket(core)
{
    OnRecvFrom.Attach<MIO>(this,&MIO::ParseDatagram);
}

MIO::~MIO()
{

}

void MIO::ParseDatagram(uint8_t * data, ssize_t &size, struct sockaddr *addr, socklen_t &socklen)
{
    MMessageType type;
    uint8_t conn_id;
    ssize_t new_size;
    
    if (socklen != sizeof(struct sockaddr_in))
    {
        LOG_WARN("IPv6 currently is not supported, datagram is ignored");
        return;
    }
    
    if (size < 2)
    {
        LOG_WARN("Invalid datagram with size " << size<< ". Skipping.");
        return;
    }
    
    conn_id = Utils::GetByte(data);
    type = (MMessageType)Utils::GetByte(&data[1]);
    new_size = size - 2;
    
    if (new_size != 0)
        ParseValidDatagram(conn_id,type,&data[2],new_size,(struct sockaddr_in *)addr);
    else
        ParseValidDatagram(conn_id,type,NULL,new_size,(struct sockaddr_in *)addr);
}