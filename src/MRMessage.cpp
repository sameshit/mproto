//
//  MRMessage.cpp
//  mproto
//
//  Created by Oleg on 14.10.12.
//
//

#include "MRMessage.h"

using namespace CoreObjectLib;
using namespace MProtoLib;

MRMessage::MRMessage(CoreObject *core,uint32_t order_id,uint8_t *data,ssize_t size)
:_core(core),_order_id(order_id),_size(size),_sent_time(0)
{
    if (_size != 0)
    {
        fast_alloc(_data, size);
        memcpy(_data,data,size);
    }
}

MRMessage::MRMessage(CoreObject *core,uint32_t order_id,uint8_t *data,ssize_t size,uint64_t sent_time)
:_core(core),_order_id(order_id),_size(size),_sent_time(sent_time)
{
    if (_size != 0)
    {
        fast_alloc(_data, size);
        memcpy(_data,data,size);
    }
}

MRMessage::~MRMessage()
{
    if (_size != 0)
        fast_free(_data);
}

bool MRMessage::CompareIds(const uint32_t &id1,const uint32_t &id2)
{
    return (id2-id1)<(id1-id2);
}