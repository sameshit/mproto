//
//  MHandshake.h
//  mproto
//
//  Created by Oleg on 11.10.12.
//
//

#ifndef __mproto__MHandshake__
#define __mproto__MHandshake__

#include "MIO.h"
#include "MSession.h"

namespace MProtoLib
{
    typedef std::unordered_map<uint64_t,MSession *> MSessionMap;
    typedef std::unordered_map<uint64_t,MSession *>::iterator MSessionMapIterator;
    typedef CoreObjectLib::Event<MProtoLib::MSession *> DisconnectEvent;
    typedef CoreObjectLib::Event<MProtoLib::MSession *> ConnectEvent;
    typedef CoreObjectLib::Event<MProtoLib::MSession *,uint8_t*,ssize_t> MessageEvent;
    
    class MHandshake
    :public MIO
    {
    public:
        MHandshake(CoreObjectLib::CoreObject *core);
        virtual ~MHandshake();
        
        void Connect(const char *ip, uint16_t port);
        void Connect(const struct sockaddr_in *);
        bool Disconnect(const char *ip, uint16_t port);
        bool Disconnect(MSession *);
        
        inline MSessionMapIterator SessionsBegin() {return _sessions_by_addr.begin();}
        inline MSessionMapIterator SessionsEnd() {return _sessions_by_addr.end();}
        inline ssize_t SessionsSize() {return _sessions_by_addr.size();}
        inline MSessionMapIterator FindSession(const uint64_t& addr)
                                    {return _sessions_by_addr.find(addr);}
        
        
        DisconnectEvent OnDisconnect;
        ConnectEvent    OnConnect;
    protected:
        void Disconnect(const MSessionMapIterator &it);
        void ParseValidDatagram(uint8_t &conn_id,MMessageType &type,uint8_t *data, ssize_t &size, struct sockaddr_in *addr);
        virtual void ParseSystemDatagram(MMessageType &,uint8_t *,ssize_t &, MSession *) = 0;
        
        bool SystemSend(MSession *session,MMessageType type,uint8_t* data1, ssize_t size1,
                        uint8_t* data2,ssize_t size2,uint8_t *data3,ssize_t size3,
                        uint8_t *data4,ssize_t size4);
        bool SystemSend(MSession *session,MMessageType type,uint8_t* data1, ssize_t size1,
                        uint8_t* data2,ssize_t size2,uint8_t *data3,ssize_t size3);
        bool SystemSend(MSession *session,MMessageType type,uint8_t* data1, ssize_t size1,
                        uint8_t* data2,ssize_t size2);
        bool SystemSend(MSession *session,MMessageType type,uint8_t* data, ssize_t size);
        bool SystemSend(MSession *session,MMessageType type);
    private:
        MSessionMap _sessions_by_addr;
    };
    
    #include "MHandshake.inl"
}


#endif /* defined(__mproto__MHandshake__) */
