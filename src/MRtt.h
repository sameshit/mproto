//
//  MRtt.h
//  mproto
//
//  Created by Oleg on 11.06.13.
//
//

#ifndef __mproto__MRtt__
#define __mproto__MRtt__

#include "MNonReliable.h"

namespace MProtoLib
{
    typedef CoreObjectLib::Event<MSession *> RttEvent;
    
    class MRtt
    :public MNonReliable
    {
    public:
        MRtt(CoreObjectLib::CoreObject *core);
        virtual ~MRtt();
        
        RttEvent OnRttUpdate;
    protected:
        void MProtoStopRttTimer();
    private:
        CoreObjectLib::Timer *_send_rtt_timer;
        CoreObjectLib::COTime _begin_rtt_time;
        
        void ProcessRttReq(uint8_t *data,ssize_t &size,MSession *session);
        void ProcessRttRep(uint8_t *data,ssize_t &size,MSession *session);
        void SendRttReq();
    };
}

#endif /* defined(__mproto__MRtt__) */
