//
//  MReliable.h
//  mproto
//
//  Created by Oleg on 14.10.12.
//
//

#ifndef __mproto__MReliable__
#define __mproto__MReliable__

#include "MOrder.h"

namespace MProtoLib
{    
    class MReliable
    :public MOrder
    {
    private:
        void ProcessAck(uint8_t *data,ssize_t &size, MSession *session);
        void CheckReliability();
        CoreObjectLib::Timer *_reliable_timer;
    public:
        MReliable(CoreObjectLib::CoreObject *core);
        virtual ~MReliable();
        
        void SendReliable(MSession *session, uint8_t *data, uint16_t size);
    };
}

#endif /* defined(__mproto__MReliable__) */
