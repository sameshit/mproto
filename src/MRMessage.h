//
//  MRMessage.h
//  mproto
//
//  Created by Oleg on 14.10.12.
//
//

#ifndef __mproto__MRMessage__
#define __mproto__MRMessage__

#include "../../CoreObject/src/CoreObject.h"

namespace MProtoLib
{
    class MRMessage
    {
    private:
        CoreObjectLib::CoreObject   *_core;
        uint8_t                     *_data;
        ssize_t                     _size;
        uint32_t                    _order_id;
        uint64_t                    _sent_time;
        
        friend class MOrder;
        friend class MReliable;
    public:
        MRMessage(CoreObjectLib::CoreObject *core,uint32_t order_id,uint8_t *data,ssize_t size);
        MRMessage(CoreObjectLib::CoreObject *core,uint32_t order_id,uint8_t *data,ssize_t size,uint64_t sent_time);
        virtual ~MRMessage();
        
        static bool CompareIds(const uint32_t &id1,const uint32_t &id2);
        
        friend struct OrderIdLess;
    };
    
    struct OrderIdLess : public std::binary_function<MRMessage *,MRMessage* , bool>
	{
		bool operator()(const MRMessage* msg1, const MRMessage* msg2) const
		{
            return MRMessage::CompareIds(msg1->_order_id, msg2->_order_id);
		}
	};
    
    struct OrderIntIdLess : public std::binary_function<uint32_t,uint32_t , bool>
	{
		bool operator()(const uint32_t &id1, const uint32_t &id2) const
		{
            return MRMessage::CompareIds(id1, id2);
		}
	};
    
    struct OrderIntIdLessOrEqual : public std::binary_function<uint32_t,uint32_t , bool>
	{
		bool operator()(const uint32_t &id1, const uint32_t &id2) const
		{
            if (id1==id2)
                return true;
            else
                return MRMessage::CompareIds(id1, id2);
		}
	};
}

#endif /* defined(__mproto__MRMessage__) */
