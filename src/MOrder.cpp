//
//  MOrder.cpp
//  mproto
//
//  Created by Oleg on 14.10.12.
//
//

#include "MOrder.h"

using namespace CoreObjectLib;
using namespace std;
using namespace MProtoLib;

MOrder::MOrder(CoreObject *core)
:MTimeout(core)
{
    
}

MOrder::~MOrder()
{

}

void MOrder::ProcessReliable(uint8_t *data, ssize_t &size, MProtoLib::MSession *session)
{
    uint32_t order_id;
    set<MRMessage*>::iterator it,tmp_it;
    MRMessage *new_msg,*old_msg;
    
    if (size <= 4)
    {
        session->GetDisconnectReason()<< "Invalid size("<<size<<") of reliable message";
        Disconnect(session);
        return;
    }
    
    order_id = Utils::GetBe32(data);
    SystemSend(session, MTAck, data, 4);
    
    if (MRMessage::CompareIds(order_id, session->_recv_id + 1))
    {
#ifdef MPROTO_DEBUG
        LOG "Received duplicate id from "<<session->GetIP()<<":"<<session->GetPort() EL;
#endif
        return;
    }
    else if (session->_recv_id + 1 == order_id)
    {
        OnReliableMessage(session,&data[4],size - 4);
        ++session->_recv_id;
        for (it = session->_unordered_messages.begin() ; it != session->_unordered_messages.end() && session->_recv_id + 1 == (*it)->_order_id; )
        {
            OnReliableMessage(session,(*it)->_data,(*it)->_size);
            ++session->_recv_id;
            tmp_it = it;
            ++it;
            old_msg = (*tmp_it);
            fast_delete(old_msg);
            session->_unordered_messages.erase(tmp_it);
        }
    }
    else
    {
#ifdef MPROTO_DEBUG
        LOG "Received out of order id("<<order_id<<"). last recv id is "<<session->_recv_id EL;
        it = session->_unordered_messages.begin();
        if (it != session->_unordered_messages.end())
        {
            LOG "Next known id is:"<<(*it)->_order_id EL;
        }
#endif
        fast_new(new_msg,_core,order_id,&data[4],size - 4);
        session->_unordered_messages.insert(new_msg);
    }
}