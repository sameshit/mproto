//
//  MTimeout.h
//  mproto
//
//  Created by Oleg on 14.10.12.
//
//

#ifndef __mproto__MTimeout__
#define __mproto__MTimeout__

#include "MPing.h"

namespace MProtoLib
{
    class MTimeout
    :public MPing
    {
    private:
        friend class MProto;
        
        CoreObjectLib::Timer *_timeout_timer;
        void CheckTimeouts();
        void UpdateReadTime(MSession *session);
        void MProtoStopTimeoutTimer();
    public:
        MTimeout(CoreObjectLib::CoreObject *core);
        virtual ~MTimeout();
    };
    
    inline void MTimeout::UpdateReadTime(MSession *session)
    {
        session->_read_time.Update();
    }
}

#endif /* defined(__mproto__MTimeout__) */
