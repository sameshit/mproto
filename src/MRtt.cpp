//
//  MRtt.cpp
//  mproto
//
//  Created by Oleg on 11.06.13.
//
//

#include "MRtt.h"

using namespace MProtoLib;
using namespace CoreObjectLib;
using namespace std;

MRtt::MRtt(CoreObject *core)
:MNonReliable(core)
{
    fast_new(_send_rtt_timer,core,1000);
    _send_rtt_timer->OnTimer.Attach(this,&MRtt::SendRttReq);
}

MRtt::~MRtt()
{
    MProtoStopRttTimer();
}   

void MRtt::MProtoStopRttTimer()
{
    if (_send_rtt_timer != NULL)
    {
        fast_delete(_send_rtt_timer);
        _send_rtt_timer = NULL;
    }
}

void MRtt::SendRttReq()
{
    _begin_rtt_time.Update();
    
    for (auto it = SessionsBegin();
         it != SessionsEnd();
         ++it)
        SystemSend((*it).second, MTRttReq);
}

void MRtt::ProcessRttReq(uint8_t *data, ssize_t &size, MProtoLib::MSession *session)
{
    if (size != 0)
    {
        session->GetDisconnectReason() << "Invalid size("<<size<<") of rtt rep mesage";
        Disconnect(session);
        return;
    }
    SystemSend(session, MTRttRep);
}

void MRtt::ProcessRttRep(uint8_t *data, ssize_t &size, MProtoLib::MSession *session)
{
    COTime now;
    
    if (now > _begin_rtt_time)
    {
        session->_rtt = (uint32_t)(now-_begin_rtt_time);
        if(session->_rtt > 1000)
            session->_rtt = 1000;
        
        session->_timeout = session->_rtt * 2;
        if (session->_timeout < 500)
            session->_timeout = 500;
        
        OnRttUpdate(session);
    }
}