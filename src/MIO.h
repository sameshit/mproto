#ifndef __mproto__MIO__
#define __mproto__MIO__

#include "../../CoreObject/src/CoreObject.h"
#include "../../CoreObject/src/core/protocols/UDP/UdpSocket.h"

#define PRINT_SESSION_ADDR(ses) ses->GetIP()<<":"<<ses->GetPort() 
#define MPROTO_MESSAGE_PROC(name) void name(MProtoLib::MSession *session,uint8_t *data,ssize_t size)
#define MPROTO_CONNECT_PROC(name) void name(MProtoLib::MSession *session)
#define MPROTO_DISCONNECT_PROC(name) MPROTO_CONNECT_PROC(name)
#define MPROTO_MESSAGE_VPROC(name) virtual MPROTO_MESSAGE_PROC(name) = 0
#define MPROTO_CONNECT_VPROC(name) virtual MPROTO_CONNECT_PROC(name) = 0
#define MPROTO_DISCONNECT_VPROC(name) virtual MPROTO_DISCONNECT_PROC(name) = 0

namespace MProtoLib
{
    enum MMessageType
    {
        MTPing       = 1,
        MTAck,
        MTRMsg,
        MTNMsg,
        MTRttReq,
        MTRttRep,
    };
    

    
    
    class MIO
    :public CoreObjectLib::UdpSocket
    {
    public:
        MIO(CoreObjectLib::CoreObject* core);
        virtual ~MIO();
    protected:
        virtual void ParseValidDatagram(uint8_t &,MMessageType &,uint8_t *, ssize_t &, struct sockaddr_in *) = 0;
    private:
        void ParseDatagram(uint8_t *, ssize_t &, struct sockaddr *, socklen_t &);
    };
}


#endif /* defined(__mproto__MIO__) */
