//
//  DoubleDisconnectTest.h
//  mproto
//
//  Created by Oleg on 28.11.12.
//
//

#ifndef __mproto__DoubleDisconnectTest__
#define __mproto__DoubleDisconnectTest__

#include "../src/MProto.h"

namespace MProtoLib
{
    class DoubleDisconnectTest
    {
    private:
        CoreObjectLib::CoreObject *_core;
        CoreObjectLib::TimeTask _disconnect_task;
        MProto *_server,*_client1,*_client2;
        
        void ProcessConnected(MSession *session);
        void ProcessDisconnected(MSession *session);
        void ProcessDisconnectTask();
    public:
        DoubleDisconnectTest();
        virtual ~DoubleDisconnectTest();
        
        void Run();
    };
}

#endif /* defined(__mproto__DoubleDisconnectTest__) */
