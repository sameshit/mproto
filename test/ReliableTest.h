//
//  ReliableTest.h
//  mproto
//
//  Created by Oleg on 14.10.12.
//
//

#ifndef __mproto__ReliableTest__
#define __mproto__ReliableTest__

#include "../src/MProto.h"

class ReliableTest
{
private:
    uint32_t _counter;
    CoreObjectLib::CoreObject *_core;
    MProtoLib::MProto *_reliable1,*_reliable2;
    CoreObjectLib::Timer      *_send_timer;
    CoreObjectLib::TimeTask   _connection_task;
    MProtoLib::MSession *_session;
    void SendHello();
    void MakeConnection();
    void Connected1(MProtoLib::MSession *session);
    void Connected2(MProtoLib::MSession *session);
    void Disconnected1(MProtoLib::MSession *session);
    void Disconnected2(MProtoLib::MSession *session);
    void Message1(MProtoLib::MSession *session,  uint8_t *data,  ssize_t size);
    void Message2(MProtoLib::MSession *session,  uint8_t *data,  ssize_t size);
    void RttUpdate1(MProtoLib::MSession *session);
    void RttUpdate2(MProtoLib::MSession *session);
public:
    ReliableTest();
    virtual ~ReliableTest();
    
    void Run();
};

#endif /* defined(__mproto__ReliableTest__) */
