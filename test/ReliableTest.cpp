//
//  ReliableTest.cpp
//  mproto
//
//  Created by Oleg on 14.10.12.
//
//

#include "ReliableTest.h"

using namespace std;
using namespace CoreObjectLib;
using namespace MProtoLib;

ReliableTest::ReliableTest()
:_counter(0)
{
    _core = new CoreObject;
    
    fast_new(_reliable1,_core);
    fast_new(_reliable2,_core);
    fast_new(_send_timer,_core,1000);
    _send_timer->OnTimer.Attach(this, &ReliableTest::SendHello);
    
    _connection_task.OnTimeTask.Attach(this, &ReliableTest::MakeConnection);
    _reliable1->OnConnect.Attach(this, &ReliableTest::Connected1);
    _reliable2->OnConnect.Attach(this, &ReliableTest::Connected2);
    _reliable1->OnDisconnect.Attach(this, &ReliableTest::Disconnected1);
    _reliable2->OnDisconnect.Attach(this, &ReliableTest::Disconnected2);
    
    _reliable1->OnReliableMessage.Attach(this, &ReliableTest::Message1);
    _reliable2->OnReliableMessage.Attach(this, &ReliableTest::Message2);
    
    _reliable1->OnRttUpdate.Attach(this,&ReliableTest::RttUpdate1);
    _reliable2->OnRttUpdate.Attach(this,&ReliableTest::RttUpdate2);
}

ReliableTest::~ReliableTest()
{
    fast_delete(_send_timer);
    fast_delete(_reliable1);
    fast_delete(_reliable2);
    
    delete _core;
}

void ReliableTest::Run()
{
    _reliable1->Bind(14345);
    _reliable2->Bind(14346);
    
    _core->GetScheduler()->Add(&_connection_task);
}

void ReliableTest::MakeConnection()
{
    _reliable1->Connect("127.0.0.1", 14346);
}

void ReliableTest::Connected1(MProtoLib::MSession *session)
{
    _session = session;
    LOG_VERBOSE("Connected 1: "<<session->GetIP()<<":"<<session->GetPort());
}

void ReliableTest::Connected2(MProtoLib::MSession *session)
{
    LOG_VERBOSE("Connected 2: "<<session->GetIP()<<":"<<session->GetPort());
}

void ReliableTest::Disconnected1(MProtoLib::MSession *session)
{
    _session = NULL;
    LOG_VERBOSE("Disconnected 1: "<<session->GetIP()<<":"<<session->GetPort()<<" with reason: "<<session->GetDisconnectReason().str());
}

void ReliableTest::Disconnected2(MProtoLib::MSession *session)
{
    LOG_VERBOSE("Disconnected 2: "<<session->GetIP()<<":"<<session->GetPort()<<" with reason: "<<session->GetDisconnectReason().str());
}

void ReliableTest::Message1(MProtoLib::MSession *session,  uint8_t *data,ssize_t size)
{
    string str((char*)data,size);
    
    LOG_VERBOSE("Message 1:" << str <<" from "<<session->GetIP()<<":" << session->GetPort());
}

void ReliableTest::Message2(MProtoLib::MSession *session,  uint8_t *data,ssize_t size)
{
    string str;
    
    str.assign((char*)data,size);
    
    LOG_VERBOSE(str);
}

void ReliableTest::SendHello()
{
    stringstream stream;
    stream << ++_counter;
   
    if (_session != NULL)
    {
        _reliable1->SendReliable(_session, (uint8_t *)stream.str().c_str(), stream.str().length());
    }
}

void ReliableTest::RttUpdate1(MProtoLib::MSession *session)
{
    LOG_VERBOSE("Rtt of session1: "<<session->GetRtt()<<", timeout "<<session->GetConnectionTimeout());
}

void ReliableTest::RttUpdate2(MProtoLib::MSession *session)
{
    LOG_VERBOSE("Rtt of session2: "<<session->GetRtt()<<", timeout "<<session->GetConnectionTimeout());
}