//
//  DoubleDisconnectTest.cpp
//  mproto
//
//  Created by Oleg on 28.11.12.
//
//

#include "DoubleDisconnectTest.h"

using namespace MProtoLib;
using namespace CoreObjectLib;
using namespace std;

DoubleDisconnectTest::DoubleDisconnectTest()
{
    _core = new CoreObject;

    fast_new(_server,_core);
    fast_new(_client1,_core);
    fast_new(_client2,_core);

    _server->OnConnect.Attach(this, &DoubleDisconnectTest::ProcessConnected);
    _server->OnDisconnect.Attach(this, &DoubleDisconnectTest::ProcessDisconnected);

    _disconnect_task.OnTimeTask.Attach(this, &DoubleDisconnectTest::ProcessDisconnectTask);
    _disconnect_task.SetTimeout(500);
}

DoubleDisconnectTest::~DoubleDisconnectTest()
{
    fast_delete(_server);
    fast_delete(_client1);
    fast_delete(_client2);

    delete _core;
}

void DoubleDisconnectTest::Run()
{
    assert(_server->Bind(12389));
    assert(_client1->Bind(12388));
    assert(_client2->Bind(12387));

    _client1->Connect("127.0.0.1", 12389);
    _client2->Connect("127.0.0.1", 12389);

    _core->GetScheduler()->Add(&_disconnect_task);
}

void DoubleDisconnectTest::ProcessDisconnected(MProtoLib::MSession *session)
{
    LOG_VERBOSE(session->GetIP()<<":"<<session->GetPort() <<" disconnected");
}

void DoubleDisconnectTest::ProcessConnected(MProtoLib::MSession *session)
{
    LOG_VERBOSE(session->GetIP()<<":"<<session->GetPort() <<" connected");
}

void DoubleDisconnectTest::ProcessDisconnectTask()
{
    _client1->Disconnect("127.0.0.1", 12389);
    _client2->Disconnect("127.0.0.1", 12389);
}
