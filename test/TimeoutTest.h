//
//  TimeoutTest.h
//  mproto
//
//  Created by Oleg on 14.10.12.
//
//

#ifndef __mproto__TimeoutTest__
#define __mproto__TimeoutTest__

#include "../src/MProto.h"

class TimeoutTest
{
private:
    CoreObjectLib::CoreObject *_core;
    MProtoLib::MProto *_timeout1,*_timeout2;
    CoreObjectLib::TimeTask _connection_task,_disconnect_task;
    
    void MakeConnection();
    void Disconnect();
    void Connected1(MProtoLib::MSession *session);
    void Connected2(MProtoLib::MSession *session);
    void Disconnected1(MProtoLib::MSession *session);
    void Disconnected2(MProtoLib::MSession *session);
public:
    TimeoutTest();
    virtual ~TimeoutTest();
    
    void Run();
};

#endif /* defined(__mproto__TimeoutTest__) */
