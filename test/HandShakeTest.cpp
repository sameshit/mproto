//
//  HandShakeTest.cpp
//  mproto
//
//  Created by Oleg on 13.10.12.
//
//

#include "HandShakeTest.h"

using namespace MProtoLib;
using namespace CoreObjectLib;
using namespace std;

HandShakeTest::HandShakeTest()
{
    _core = new CoreObject;
    
    fast_new(_shake1,_core);
    fast_new(_shake2,_core);
    
    _shake1->OnConnect.Attach(this, &HandShakeTest::Connected1);
    _shake2->OnConnect.Attach(this, &HandShakeTest::Connected2);
    _shake1->OnDisconnect.Attach(this, &HandShakeTest::Disconnected1);
    _shake2->OnDisconnect.Attach(this, &HandShakeTest::Disconnected2);
    _connect_task.OnTimeTask.Attach(this, &HandShakeTest::MakeConnection);
}

HandShakeTest::~HandShakeTest()
{
    fast_delete(_shake1);
    fast_delete(_shake2);
    
    delete _core;
}

bool HandShakeTest::Run()
{
    if (!_shake1->Bind(12345))
        return false;
    
    if (!_shake2->Bind(12346))
        return false;
    
    _core->GetScheduler()->Add(&_connect_task);
    
    return true;
}

void HandShakeTest::Connected1(MProtoLib::MSession *session)
{
    LOG_VERBOSE( "Connected 1: "<<session->GetIP() << ":" << session->GetPort() );
}

void HandShakeTest::Disconnected1(MProtoLib::MSession *session)
{
    LOG_VERBOSE( "Disconnected 1: "<<session->GetIP() << ":" << session->GetPort());
}

void HandShakeTest::Connected2(MProtoLib::MSession *session)
{
    LOG_VERBOSE( "Connected 2: "<<session->GetIP() << ":" << session->GetPort());

    _connect_task.SetTimeout(1000);
    _core->GetScheduler()->Add(&_connect_task);    
}

void HandShakeTest::Disconnected2(MProtoLib::MSession *session)
{
    LOG_VERBOSE("Disconnected 2: "<<session->GetIP() << ":" << session->GetPort());
}

void HandShakeTest::MakeConnection()
{
    LOG_VERBOSE("Make connection");

    if (!_shake1->Disconnect("127.0.0.1",12346))
    {
        LOG_VERBOSE( "Cannot disconnect" );
    }
    _shake1->Connect("127.0.0.1", 12346);
}