//
//  TimeoutTest.cpp
//  mproto
//
//  Created by Oleg on 14.10.12.
//
//

#include "TimeoutTest.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

TimeoutTest::TimeoutTest()
{
    _core = new CoreObject;
    
    fast_new(_timeout1, _core);
    fast_new(_timeout2, _core);
    
    _connection_task.OnTimeTask.Attach(this, &TimeoutTest::MakeConnection);
    
    _disconnect_task.SetTimeout(1000);
    _disconnect_task.OnTimeTask.Attach(this, &TimeoutTest::Disconnect);
    
    _timeout1->OnConnect.Attach(this, &TimeoutTest::Connected1);
    _timeout2->OnConnect.Attach(this, &TimeoutTest::Connected2);
    
    _timeout1->OnDisconnect.Attach(this, &TimeoutTest::Disconnected1);
    _timeout2->OnDisconnect.Attach(this, &TimeoutTest::Disconnected2);
}

TimeoutTest::~TimeoutTest()
{
    fast_delete(_timeout2);
    fast_delete(_timeout1);
    
    delete _core;
}

void TimeoutTest::Run()
{
    _timeout1->Bind(12345);
    _timeout2->Bind(12346);
    
    _core->GetScheduler()->Add(&_connection_task);
}

void TimeoutTest::MakeConnection()
{
    _timeout1->Connect("127.0.0.1", 12346);
}

void TimeoutTest::Disconnect()
{
    _timeout1->Disconnect("127.0.0.1", 12346);
}

void TimeoutTest::Connected1(MProtoLib::MSession *session)
{
    LOG_VERBOSE("Connected 1: " << session->GetIP() <<":"<<session->GetPort());
}

void TimeoutTest::Connected2(MProtoLib::MSession *session)
{
    LOG_VERBOSE("Connected 2: " << session->GetIP() <<":"<<session->GetPort());
    _core->GetScheduler()->Add(&_disconnect_task);
}

void TimeoutTest::Disconnected1(MProtoLib::MSession *session)
{
    LOG_VERBOSE("Disconnected 1:" << session->GetIP() << ":" << session->GetPort());
}

void TimeoutTest::Disconnected2(MProtoLib::MSession *session)
{
    LOG_VERBOSE("Disconnected 2:" << session->GetIP() << ":" << session->GetPort());
}