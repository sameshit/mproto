//
//  HandShakeTest.h
//  mproto
//
//  Created by Oleg on 13.10.12.
//
//

#ifndef __mproto__HandShakeTest__
#define __mproto__HandShakeTest__

#include "../src/MProto.h"

class HandShakeTest
{
private:
    CoreObjectLib::CoreObject * _core;
    MProtoLib::MProto *_shake1,*_shake2;
    
    
    void MakeConnection();
    void Connected1(MProtoLib::MSession *);
    void Connected2(MProtoLib::MSession *);
    void Disconnected1(MProtoLib::MSession *);
    void Disconnected2(MProtoLib::MSession *);
    
    CoreObjectLib::TimeTask _connect_task;
    
public:
    HandShakeTest();
    virtual ~HandShakeTest();
    
    bool Run();
};

#endif /* defined(__mproto__HandShakeTest__) */
